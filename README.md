<!-- Intro -->
<div align="center">
<h2>
        <samp>&gt; Hey There!, I am
                <strong><a target="_blank" href="#">Samiul Azim</a></strong>
        </samp>
</h2>
</div>
<div align="center">
        <samp>
            <strong>「 A Software Engineer in Making 」</strong>
        </samp>
</div>

<div align="center">
<p>
        <!-- Organisation  -->
        <samp>
                <br>
                「 I am from Bangladesh and currently I am pursuing my bachelor degree majoring in Software Technology. I am a passionate learner who's always willing to learn and work across technologies and domains. I love to explore new technologies and leverage them to solve real-life problems.</b> 」
                <br>
                <br>
        </samp>
</p>
</div>

<hr/>

<img height="160" align="right" alt="Night Coding" src="https://gitlab.com/samiulazim/samiulazim/-/raw/main/images/night-coding.gif"/>


- 🏢 Currently I'm working at [**Open Source Projects**](https://gitlab.com/users/samiulazim/projects).
- ⚙️ I use daily: `.c`, `.cpp`, `.js`, `.html`, `.css`, `.py`
- 💻 I'm mostly active within the Comitative Programming Community.
- 🌱 Learning Software Development.
- 🐧 GNU/Linux Enthusiast.
- 📄 Please have a look at my [**Resume**](#) for more details about me.
- 😊 I'm open to feedback and suggestions!


<div align="center">
<a href="https://linkedin.com/in/samiulazim"><img src="https://img.shields.io/badge/-LinkedIn-0077B5?style=for-the-badge&logo=Linkedin&logoColor=white"/></a>
<a href="mailto:contact.samiulazim@gmail.com"><img src="https://img.shields.io/badge/-Gmail-D14836?style=for-the-badge&logo=Gmail&logoColor=white"/></a>
<a target="_blank" href="https://twitter.com/w3samiulazim"><img src="https://img.shields.io/badge/-Twitter-1DA1F2?style=for-the-badge&logo=Twitter&logoColor=white"/></a>
<a target="_blank" href="https://instagram.com/w3samiulazim"><img src="https://img.shields.io/badge/-Instagram-E4405F?style=for-the-badge&logo=Instagram&logoColor=white"/></a>
<a target="_blank" href="https://facebook.com/w3samiulazim"><img src="https://img.shields.io/badge/-Facebook-1877F2?style=for-the-badge&logo=Facebook&logoColor=white"/></a>
<a href="mailto:samiulazim@protonmail.com"><img src="https://img.shields.io/badge/-ProtonMail-8B89CC?style=for-the-badge&logo=ProtonMail&logoColor=white"/></a>
</div>

<hr/>

<!-- Details Section -->
<details open>
    <summary><strong><samp>&#9776; Technologies & Tools...</samp></strong></summary>
        <br>
<div align="center">
        <!-- Programming Languages -->
        <!-- C -->
        <img src="https://img.shields.io/badge/-C-00599C?style=flat-square&logo=c&logoColor=white">
        <!-- C++ -->
        <img src="https://img.shields.io/badge/-C%2B%2B-00599C?style=flat-square&logo=C%2B%2B&logoColor=white">
        <!-- JavaScript -->
        <img src="https://img.shields.io/badge/-JavaScript-323330?style=flat-square&logo=JavaScript&logoColor=F7DF1E">
        <!-- TypeScript -->
        <img src="https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=JavaScript&logoColor=white">
        <!-- Python -->
        <img src="https://img.shields.io/badge/-Python-3776AB?style=flat-square&logo=Python&logoColor=white">
        <!-- HTML -->
        <img src="https://img.shields.io/badge/-HTML-E34F26?style=flat-square&logo=HTML5&logoColor=white">
        <!-- CSS -->
        <img src="https://img.shields.io/badge/-CSS-1572B6?style=flat-square&logo=CSS3&logoColor=white">
        <!-- SASS -->
        <img src="https://img.shields.io/badge/-Sass-CC6699?style=flat-square&logo=sass&logoColor=white">
        <!-- React Js -->
        <img src="https://img.shields.io/badge/React-20232A?style=flat-square&logo=react&logoColor=61DAFB">
        <!-- Node Js -->
        <img src="https://img.shields.io/badge/Node.js-339933?style=flat-square&logo=nodedotjs&logoColor=white">
        <!-- Express Js -->
        <img src="https://img.shields.io/badge/Express.js-000000?style=flat-quare&logo=express&logoColor=white">
        <!-- MongoDB -->
        <img src="https://img.shields.io/badge/MongoDB-4EA94B?style=flat-square&logo=mongodb&logoColor=white">
        <!-- MySQL -->
        <img src="https://img.shields.io/badge/MySQL-005C84?style=flat-square&logo=mysql&logoColor=white">
        <!-- Shell Script -->
        <img src="https://img.shields.io/badge/Shell_Script-121011?style=flat-square&logo=gnu-bash&logoColor=white">
        <!-- Shell Script -->
        <img src="https://img.shields.io/badge/Netlify-00C7B7?style=flat-square&logo=netlify&logoColor=white">
        <!-- GitLab Pages -->
        <img src="https://img.shields.io/badge/GitLab_Pages-330F63?style=flat-square&logo=gitlab&logoColor=white">
        <!-- GitHub Pages -->
        <img src="https://img.shields.io/badge/GitHub_Pages-100000?style=flat-square&logo=github&logoColor=white">
        <!-- Linux -->
        <img src="https://img.shields.io/badge/Linux-FCC624?style=flat-square&logo=linux&logoColor=black">
        <!-- Neovim -->
        <img src="https://img.shields.io/badge/NeoVim-%2357A143.svg?&style=flat-square&logo=neovim&logoColor=white">
        <!-- VS Code -->
        <img src="https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=flat-square&logo=visual%20studio%20code&logoColor=white">
</div>
</details>

<hr/>

<!-- Footer -->
<samp>
    <p align="center">
        ════ ⋆★⋆ ════
        <br>
        "Happy Coding!"
    </p>
</samp>
